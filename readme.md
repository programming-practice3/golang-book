# Golang Book

Code on the concepts from important chapters of the golang book.

## Resources

[An Introduction to Programming in Go by Caleb Doxsey](https://www.golang-book.com/books/intro)

