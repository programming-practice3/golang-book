# Pointers

Utilize pointers in go. Operators, * and &, and the new function can be used to create a pointer.

## Resources

[Pointers](https://www.golang-book.com/books/intro/8)

